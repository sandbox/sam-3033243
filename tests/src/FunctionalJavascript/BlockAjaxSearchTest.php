<?php

namespace Drupal\Tests\layout_builder_block_search\FunctionalJavascript;

use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test searching for blocks with AJAX.
 *
 * @group layout_builder_block_search
 */
class BlockAjaxSearchTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'node',
    'datetime',
    'layout_builder',
    'user',
    'layout_builder_test',
    'block_content',
    'layout_builder_block_search',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $user = $this->drupalCreateUser([
      'configure any layout',
      'administer node display',
      'administer node fields',
    ]);
    $user->save();
    $this->drupalLogin($user);
    $this->createContentType(['type' => 'bundle_with_section_field']);

    // Create 101 test blocks.
    BlockContentType::create([
      'id' => 'basic',
      'label' => 'basic',
    ])->save();
    foreach (range(0, 100) as $i) {
      BlockContent::create([
        'info' => 'Block ' . $i,
        'type' => 'basic',
      ])->save();
    }
  }

  /**
   * Test filtering a block section using AJAX.
   */
  public function testAddAjaxBlock() {
    $node = $this->createNode([
      'type' => 'bundle_with_section_field',
      'body' => 'The node body',
    ]);
    $node->save();

    $this->drupalPostForm('admin/structure/types/manage/bundle_with_section_field/display/default', ['layout[enabled]' => TRUE], 'Save');

    $this->clickLink('Manage layout');
    $this->clickLink('Add Block');
    $this->assertSession()->waitForElementVisible('css', '#custom-choose-block-links');

    $this->assertSession()->linkExists('Block 0');
    $this->assertSession()->linkExists('Block 14');
    $this->assertSession()->linkNotExists('Block 15');
    $this->assertSession()->linkNotExists('Block 99');
    $this->assertSession()->pageTextContains('86 more');

    $this->getSession()->getPage()->fillField('block_categories[Custom][links][search]', 'Block 99');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->linkExists('Block 99');
    $this->assertSession()->linkNotExists('Block 0');
  }

}
