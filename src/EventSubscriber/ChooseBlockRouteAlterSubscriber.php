<?php

namespace Drupal\layout_builder_block_search\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Routing\RouteBuildEvent;

/**
 * Alters the choose block route to use the custom controller.
 */
class ChooseBlockRouteAlterSubscriber implements EventSubscriberInterface {

  /**
   * Alter routes.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The event.
   */
  public function alterRoutes(RouteBuildEvent $event) {
    foreach ($event->getRouteCollection() as $name => $route) {
      if ($name === 'layout_builder.choose_block') {
        $defaults = $route->getDefaults();
        unset($defaults['_controller']);
        $defaults['_form'] = '\Drupal\layout_builder_block_search\Controller\SearchChooseBlockController';
        $route->setDefaults($defaults);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RoutingEvents::ALTER][] = ['alterRoutes', -150];
    return $events;
  }

}
