<?php

namespace Drupal\layout_builder_block_search\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A controller overriding the core LB controller for adding blocks.
 *
 * @see \Drupal\layout_builder\Controller\ChooseBlockController
 */
class SearchChooseBlockController extends FormBase {

  use AjaxHelperTrait;
  use LayoutBuilderContextTrait;
  use StringTranslationTrait;

  /**
   * The maximum number of items to display before using AJAX.
   */
  const MAX_ITEMS_BEFORE_AJAX = 15;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ChooseBlockController constructor.
   */
  public function __construct(BlockManagerInterface $block_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->blockManager = $block_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_builder_block_search';
  }

  /**
   * Provides the UI for choosing a new block.
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = 0, $region = '') {
    // Prevent the enter key on text inputs from submitting the form.
    $form['#attributes']['onsubmit'] = 'return false';

    $form['#title'] = $this->t('Choose a block');
    if ($this->entityTypeManager->hasDefinition('block_content_type') && $types = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple()) {
      if (count($types) === 1) {
        $type = reset($types);
        $plugin_id = 'inline_block:' . $type->id();
        if ($this->blockManager->hasDefinition($plugin_id)) {
          $url = Url::fromRoute('layout_builder.add_block', [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
            'plugin_id' => $plugin_id,
          ]);
        }
      }
      else {
        $url = Url::fromRoute('layout_builder.choose_inline_block', [
          'section_storage_type' => $section_storage->getStorageType(),
          'section_storage' => $section_storage->getStorageId(),
          'delta' => $delta,
          'region' => $region,
        ]);
      }
      if (isset($url)) {
        $form['add_block'] = [
          '#type' => 'link',
          '#url' => $url,
          '#title' => $this->t('Create @entity_type', [
            '@entity_type' => $this->entityTypeManager->getDefinition('block_content')
              ->getSingularLabel(),
          ]),
          '#attributes' => $this->getAjaxAttributes(),
        ];
        $form['add_block']['#attributes']['class'][] = 'inline-block-create-button';
      }
    }

    $block_categories['#type'] = 'container';
    $block_categories['#tree'] = TRUE;
    $block_categories['#attributes']['class'][] = 'block-categories';
    $block_categories['#attributes']['class'][] = 'js-layout-builder-categories';

    // @todo Explicitly cast delta to an integer, remove this in
    //   https://www.drupal.org/project/drupal/issues/2984509.
    $delta = (int) $delta;

    $definitions = $this->blockManager->getFilteredDefinitions('layout_builder', $this->getAvailableContexts($section_storage), [
      'section_storage' => $section_storage,
      'delta' => $delta,
      'region' => $region,
    ]);
    $grouped_definitions = $this->blockManager->getGroupedDefinitions($definitions);
    foreach ($grouped_definitions as $category => $blocks) {
      $block_categories[$category]['#type'] = 'details';
      $block_categories[$category]['#attributes']['class'][] = 'js-layout-builder-category';
      $block_categories[$category]['#open'] = TRUE;
      $block_categories[$category]['#title'] = $category;

      $search_string = $form_state->getValue([
        'block_categories',
        $category,
        'links',
        'search',
      ]);
      $block_categories[$category]['links'] = $this->getBlockLinks($section_storage, $delta, $region, $blocks, $category, $search_string);
    }
    $form['block_categories'] = $block_categories;
    return $form;
  }

  /**
   * Gets a render array of block links.
   */
  protected function getBlockLinks(SectionStorageInterface $section_storage, $delta, $region, array $blocks, $category, $search_string = NULL) {
    $build = [];
    $wrapper_id = sprintf('%s-choose-block-links', Html::getId($category));

    if (count($blocks) >= static::MAX_ITEMS_BEFORE_AJAX) {
      $build['search'] = [
        '#type' => 'textfield',
        '#attributes' => [
          'placeholder' => $this->t('Search blocks'),
        ],
        '#ajax' => [
          'callback' => [static::class, 'pluginSearchAjaxCallback'],
          'event' => 'change',
          'wrapper' => $wrapper_id,
          'method' => 'replace',
        ],
      ];
    }

    if ($search_string) {
      $blocks = $this->filterBlocksBySearchString($blocks, $search_string);
    }
    $found_blocks_count = count($blocks);

    $blocks = array_slice($blocks, 0, static::MAX_ITEMS_BEFORE_AJAX);
    $displayed_blocks_count = count($blocks);

    $links = [];
    foreach ($blocks as $block_id => $block) {
      $links[] = $this->getBlockLink($section_storage, $delta, $region, $block, $block_id);
    }

    if ($found_blocks_count > $displayed_blocks_count) {
      $links[] = [
        'title' => $this->t('@count more', ['@count' => $found_blocks_count - $displayed_blocks_count]),
        'url' => Url::fromRoute('<nolink>'),
      ];
    }

    $build['links'] = [
      '#prefix' => sprintf('<div id="%s">', $wrapper_id),
      '#suffix' => '</div>',
      '#theme' => 'links',
      '#links' => $links,
    ];
    return $build;
  }

  /**
   * The AJAX callback for the search element.
   */
  public static function pluginSearchAjaxCallback($form, FormStateInterface $form_state) {
    // Replace only the section of links that the user has made a search for.
    $links_path = $form_state->getTriggeringElement()['#parents'];
    array_pop($links_path);
    $links_path[] = 'links';
    $links = NestedArray::getValue($form, $links_path);
    return $links;
  }

  /**
   * Filter some blocks by a search string.
   *
   * @param array $blocks
   *   A list of blocks.
   * @param string $search_string
   *   A search string.
   *
   * @return array
   *   A filtered list.
   */
  protected function filterBlocksBySearchString(array $blocks, $search_string) {
    return array_filter($blocks, function ($block) use ($search_string) {
      return stripos($block['admin_label'], $search_string) !== FALSE;
    });
  }

  /**
   * Get a block link.
   */
  protected function getBlockLink(SectionStorageInterface $section_storage, $delta, $region, $block, $block_id) {
    $attributes = $this->getAjaxAttributes();
    $attributes['class'][] = 'js-layout-builder-block-link';
    $link = [
      'title' => $block['admin_label'],
      'url' => Url::fromRoute('layout_builder.add_block',
        [
          'section_storage_type' => $section_storage->getStorageType(),
          'section_storage' => $section_storage->getStorageId(),
          'delta' => $delta,
          'region' => $region,
          'plugin_id' => $block_id,
        ]
      ),
      'attributes' => $attributes,
    ];
    return $link;
  }

  /**
   * Get dialog attributes if an ajax request.
   *
   * @return array
   *   The attributes array.
   */
  protected function getAjaxAttributes() {
    if ($this->isAjax()) {
      return [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'dialog',
        'data-dialog-renderer' => 'off_canvas',
      ];
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
